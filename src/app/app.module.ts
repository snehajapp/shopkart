import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { KartPageComponent } from './kart-page/kart-page.component';
import { ShowItemComponent } from './show-item/show-item.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { BankingComponent } from './banking/banking.component';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { PaymentMethodComponent } from './payment-method/payment-method.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'item', pathMatch: 'full' },
  { path: 'item', component: ShowItemComponent },
  { path: 'kart', component: KartPageComponent },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'banking', component: BankingComponent },
  { path: 'paymentMethod', component: PaymentMethodComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    ShowItemComponent,
    KartPageComponent,
    CheckoutComponent,
    BankingComponent,
    PaymentMethodComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
