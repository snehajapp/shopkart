import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {
  checkout: boolean;
  item: boolean;

  data = null;
  total = 0;

  totalAmountToPay = 0;

  constructor(private router: Router, private dataService: DataService) { }

  ngOnInit() {
    if (this.dataService.data) {
      let total = 0;
      this.data = this.dataService.data;
      
      this.data.map((item) => {
        total = total + (item.price * item.count);
      });
      this.totalAmountToPay = total;
    }

  }
  navigateTo(pagename) {
    this.checkout = true;
    this.item = true;
    this.router.navigateByUrl(pagename);
  }


}
