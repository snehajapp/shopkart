import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-show-item',
  templateUrl: './show-item.component.html',
  styleUrls: ['./show-item.component.css']
})
export class ShowItemComponent implements OnInit {

  kart: boolean;
  item: any;

  // data = {
  //   dress: {
  //     count: 0, price: 100, currency: 'INR', imgPath: ''
  //   },
  //   heels: {
  //     count: 0, price: 100, currency: 'INR', imgPath: ''
  //   },
  //   bag: {
  //     count: 0, price: 100, currency: 'INR', imgPath: ''
  //   },
  //   cosmetics: {
  //     count: 0, price: 100, currency: 'INR', imgPath: ''
  //   }
  // }


  data = [
    { name: 'dress', item: 'clothing', count: 0, price: 100, currency: 'INR', imgPath: 'url(/assets/images/dress.jpg)' },
    { name: 'heels', item: 'footwear', count: 0, price: 299, currency: 'INR', imgPath: 'url(/assets/images/heels.jpg)' },
    { name: 'bag', item: 'accesories', count: 0, price: 1490, currency: 'INR', imgPath: 'url(/assets/images/bag.jpg)' },
    { name: 'lipstick', item: 'cosmetics', count: 0, price: 100, currency: 'INR', imgPath: 'url(/assets/images/cosmetics.jpg)' },
  ]


  constructor(private router: Router, private dataService: DataService) { }

  ngOnInit() {
    console.log(this.data);
    if(this.dataService.data){
      this.data = this.dataService.data;
    }

  }

  navigateTo(pageName) {
    this.kart = true;
    this.router.navigateByUrl(pageName);
  }

  addToKart(item, itemName) {
    item[itemName]++;
    // console.log('dress');
    // if (item === 'dress') {
    //   this.data.dress.count++;
    // } else if (item === 'heels') {
    //   this.data.heels.count++;
    // } else if (item === 'bag') {
    //   this.data.bag.count++;
    // } else if (item === 'lipstick') {
    //   this.data.lipstick.count++;
    // }

    this.dataService.data = this.data;
  }

  removeFromKart(item,itemName) {
    item[itemName]--;
    // if (item === 'dress') {
    //   this.data.dress.count--;
    // }
    // if (item == 'heels') {
    //   this.data.heels.count--;
    // }
    // if (item === 'bag') {
    //   this.data.bag.count--;
    // }
    // if (item === 'lipstick') {
    //   this.data.lipstick.count--;
    // }

    this.dataService.data = this.data;


  }


}
