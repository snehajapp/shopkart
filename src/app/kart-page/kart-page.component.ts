import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-kart-page',
  templateUrl: './kart-page.component.html',
  styleUrls: ['./kart-page.component.css']
})
export class KartPageComponent implements OnInit {
  kart: boolean;

  constructor(private router:Router, private dataService:DataService) { }

  ngOnInit() {
    console.log(this.dataService.data); 
  
  }

  navigateTo(pageName){
    this.kart = true;
    this.router.navigateByUrl(pageName);
  }
}
